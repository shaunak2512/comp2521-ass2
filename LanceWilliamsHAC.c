#include "LanceWilliamsHAC.h"
#include <stdio.h>
#include <stdlib.h>
#include "PQ.h"
#include "Dijkstra.h"
#include <limits.h>
#include <string.h>

#define INF INT_MAX;
#ifndef max
    #define max(a,b) ((a) > (b) ? (a) : (b))
#endif
#ifndef min
    #define min(a,b) ((a) < (b) ? (a) : (b))
#endif
#ifndef abs
    #define abs(a) ((a) < 0 ? (-a) : (a))
#endif

void updateDist(int dim,double dist[dim][dim], int method, int a, int b);

void printDendrogram(Dendrogram dend);

int getWeight(Graph g, int src, int dest){
	AdjList l = outIncident(g,src);
	while(l != NULL){
		if(l->w == dest) return l->weight;
		l = l->next;
	}
	return 0;
}

double *findMin(int dim,double dist[dim][dim]){
	double min = LONG_MAX;
	int i,j;
	double *res;
	res = malloc(sizeof(double)*3);
	for(i=0;i<dim;i++){
		for(j=0;j<dim;j++){
			if(dist[i][j] < min){
				min = dist[i][j];
				res[0] = min;
				res[1] = i;
				res[2] = j;
			}
		}
	}
	return res;
}

void updateDist(int dim,double dist[dim][dim], int method, int a, int b){
    int i, j;
    //max(b,a);

    for(i=0;i<dim;i++){
        for(j=0;j<dim;j++){
            if(i==min(b,a)){
                if(i!=j&&j!=max(b,a)){
                    //printf("a= %d, b = %d, i= %d, j = %d\n distAJ %lf distBJ %lf\n\n",a,b,i,j,dist[a][j],dist[b][j]);
                    //to deal with the inf issues
                    if(dist[a][j]==INT_MAX||dist[b][j]==INT_MAX) dist[i][j]=(min(dist[b][j],dist[a][j]));
                    else dist[i][j]=0.5*dist[a][j]+0.5*dist[b][j]+(method==2 ? (0.5) : (-0.5))*abs((dist[a][j]-dist[b][j]));

                    //printf("distIJ %lf\n",dist[i][j]);
                }
                dist[max(b,a)][j]=INF;
            }
            if(j==min(b,a)){
                if(i!=j&&i!=max(b,a)){
                    if(dist[i][a]==INT_MAX||dist[i][b]==INT_MAX) dist[i][j]=(min(dist[i][a],dist[i][b]));
                    else dist[i][j]=0.5*dist[i][a]+0.5*dist[i][b]+(method==2 ? (0.5) : (-0.5))*abs(dist[i][a]-dist[i][b]);
                }
                dist[i][max(b,a)]=INF;
            }
        }
    }
 
}

void printDist(int dim,double dist[dim][dim]){
	int i,j;
	for(i=0; i<dim;i++){
		for(j=0;j<dim;j++){
			if(dist[i][j] == INT_MAX) 
				printf("INF      ");
			else 
				printf("%lf  ", dist[i][j]);
		}
		printf("\n");
	}
}


Dendrogram LanceWilliamsHAC(Graph g, int method){
	int nV = numVerticies(g);
	double dist[nV][nV];
	int i,j;
	//PQ q = newPQ();
	Dendrogram *d_arr = malloc(nV * sizeof(Dendrogram *));
	//Dendrogram d = NULL;
	//Populate initial dist array and d_arr
	for(i=0; i<nV; i++){
		d_arr[i] = malloc(sizeof(DNode));
		(d_arr[i])->vertex = i;
		(d_arr[i])->left = NULL;
		(d_arr[i])->right = NULL;
		for (j = 0; j < nV; j++){
			if(adjacent(g,i,j) && adjacent(g,j,i)){
				dist[i][j] = (double)(1.0/max(getWeight(g,j,i),getWeight(g,i,j)));
				//ItemPQ item; item->key = atoi("")
			}
			else if(adjacent(g,i,j)){
				dist[i][j] = (double)(1.0/getWeight(g,i,j));
			}
			else if(adjacent(g,j,i)){
				dist[i][j] = (double)(1.0/getWeight(g,j,i));
			}
			else {
				dist[i][j] = INF;
			}
			//dist[i][j] = (adjacent(g,i,j) && adjacent(g,j,i)) ? (double)(1.0/max(getWeight(g,j,i),getWeight(g,i,j))) : (adjacent(g,i,j)) ? (double)(1.0/(getWeight(g,i,j))) : INF;		
				 
		}
	}
    for (i=0;i<nV-1;i++){
        double *arr = findMin(nV,dist);
        printf("\n\n%d\n",i);
        printDist(nV,dist);
        updateDist(nV, dist, method, arr[1], arr[2]);
        Dendrogram new=malloc(sizeof(DNode));
        new->vertex=-1;
        new->left=d_arr[(int)min(arr[1], arr[2])];
        new->right=d_arr[(int)max(arr[1], arr[2])];
        d_arr[(int)min(arr[1], arr[2])]=new;
        d_arr[(int)max(arr[1], arr[2])]=NULL;
        free(arr);
        
    }
    Dendrogram toReturn=d_arr[0];
    for(i=1;i<nV;i++){
        free(d_arr[i]);
    }
    free(d_arr);
    free(NULL);

    return toReturn;
}

void printDendrogram(Dendrogram dend){
	
	if(dend == NULL) return;
	if(dend->vertex != -1){
		printf("Vertex: %d - \n",dend->vertex);
	}
	printDendrogram(dend->left);
	printf("    ");
    printDendrogram(dend->right);
}

void freeDendrogram(Dendrogram d){

	if (d == NULL) return;
	freeDendrogram(d->left);
	freeDendrogram(d->right);
    printf("%d\n",d->vertex);
	free(d);

}
