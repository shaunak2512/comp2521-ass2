
CC=gcc
CFLAGS=-Wall -Werror -g


CentralityMeasures : CentralityMeasures.o Graph.o Dijkstra.o PQ.o
						$(CC) $(CFLAGS) -o cm CentralityMeasures.o Graph.o Dijkstra.o PQ.o

CentralityMeasures.o : CentralityMeasures.c Graph.h Dijkstra.h PQ.h
						$(CC) $(CFLAGS) -c CentralityMeasures.c Graph.h Dijkstra.h PQ.h	

Dijkstra : Dijkstra.o Graph.o PQ.o  
			$(CC) $(CFLAGS) -o dj Dijkstra.o Graph.o PQ.o  
Dijkstra.o : Dijkstra.c Graph.h PQ.h
			$(CC) $(CFLAGS) -c Dijkstra.c PQ.h Graph.h
Graph.o : Graph.c Graph.h
			$(CC) $(CFLAGS) -c Graph.c
PQ.o : PQ.c PQ.h
			$(CC) $(CFLAGS) -c PQ.c	

clean: 
		rm -f *.o dj cm
