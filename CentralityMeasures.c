#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "Dijkstra.h"
#include "CentralityMeasures.h"

int countPaths(ShortestPaths sp, int src);
int countPathsThroughv(ShortestPaths sp, int src, int v,int foundV);
int sumPaths(ShortestPaths sp);
int accessibleNodes(ShortestPaths sp);

NodeValues outDegreeCentrality(Graph g){
    NodeValues outDegrees;
    outDegrees.noNodes = numVerticies(g);
	outDegrees.values = malloc(outDegrees.noNodes * sizeof(double));
	
    int i;
    //loops through all the nodes in the graph
    for(i=0;i<outDegrees.noNodes;i++){
        //gets a list of the outlinks for that node
        AdjList  OutLinks = outIncident(g,i);
        
        //loops through the outlinks and counts them
        int count=0;
        while (OutLinks!=NULL){
            count++;
            OutLinks=OutLinks->next;
        }
        outDegrees.values[i]=count;
    }
    
    return outDegrees;
}

NodeValues degreeCentrality(Graph g){
    NodeValues degrees;
    degrees.noNodes = numVerticies(g);
	degrees.values = malloc(degrees.noNodes * sizeof(double));
    
    //gets the outdegree and indegree centrality for the graph
    NodeValues Out=outDegreeCentrality(g);
    NodeValues In=inDegreeCentrality(g);

    int i;
    for(i=0;i<degrees.noNodes;i++){
        //adds the out degrees and in degrees for each node in the graph
        degrees.values[i]=Out.values[i]+In.values[i];
    }
    
    return degrees;
}


NodeValues inDegreeCentrality(Graph g){
    NodeValues inDegrees;
    inDegrees.noNodes = numVerticies(g);
	inDegrees.values = malloc(inDegrees.noNodes * sizeof(double));
	
    int i;
    //loops through all the nodes in the graph
    for(i=0;i<inDegrees.noNodes;i++){
        //gets a list of the inlinks for that node
        AdjList  inLinks = inIncident(g,i);
        
        //loops through the inlinks and counts them
        int count=0;
        while (inLinks!=NULL){
            count++;
            inLinks=inLinks->next;
        }
        inDegrees.values[i]=count;
    }
    
    return inDegrees;
}

int sumPaths(ShortestPaths sp){
    //sums the distances of all the shortest paths
    int i;
    int count=0;
    for (i=0;i<sp.noNodes;i++){
        count+=sp.dist[i];
    }
    
    return count;  
}

int accessibleNodes(ShortestPaths sp){
    //counts the number of accessible nodes from the source in the give sp
    int i;
    
    int count=1; //because it can access itself
    for (i=0;i<sp.noNodes;i++){
        if(sp.dist[i]>0) count++;
    }
    
    return count;
}


NodeValues closenessCentrality(Graph g){
	NodeValues *closeness = malloc(sizeof(NodeValues));
    closeness->noNodes = numVerticies(g);
    closeness->values = malloc(closeness->noNodes * sizeof(double));
    
    int i;
    //loops through each node in the graph
    for(i=0;i<closeness->noNodes;i++){
        ShortestPaths paths = dijkstra(g,i);
        int sumP = sumPaths(paths);
        int n = accessibleNodes(paths);
        double c=0;
        // applies the Wasserman and Faust formula, but not if the denominator would be zero
        if(sumP*closeness->noNodes>0)c=(double)(n-1)*(n-1)/(double)(closeness->noNodes-1)/sumP;
        closeness->values[i]=c;
    }
    
 	return *closeness;
}

NodeValues betweennessCentrality(Graph g){
	NodeValues *between = malloc(sizeof(NodeValues));
	int nV = numVerticies(g);
	between->noNodes = nV;
	between->values = malloc(between->noNodes * sizeof(double));
	int i;
	for(i=0;i<nV;i++) between->values[i] = 0;
	
    int v;
	for(v=0; v<nV; v++){
		//between->values[v] = 0;
		int s;
		for(s=0; s<nV; s++){
			if(s != v){
				int t;
				ShortestPaths sp = dijkstra(g,s);

				
				//double betweenness = 0;
				for(t=0; t<nV; t++){
					if(t != s && t != v){
						int noPaths = 0;
						int noPathsThroughv = 0;

						noPaths = countPaths(sp,t);

						noPathsThroughv = countPathsThroughv(sp,t,v,0);

						//Taking care of divide by zero error
						between->values[v] += (noPaths == 0) ? 0 : (double)noPathsThroughv/(double)noPaths;
					}	
				}	
			}	
		}
	
	}
	return *between;
}

NodeValues betweennessCentralityNormalised(Graph g){
	NodeValues *betweenNorm = malloc(sizeof(NodeValues));
	NodeValues between = betweennessCentrality(g);
	int nV = numVerticies(g);
	betweenNorm->noNodes = nV;
	betweenNorm->values = malloc(betweenNorm->noNodes * sizeof(double));
	int i;
	for(i=0; i<nV; i++){
		betweenNorm->values[i] = (double)(1/(((double)(nV-1))*((double)(nV-2))))*between.values[i]; 
	}
	return *betweenNorm;
}

int countPaths(ShortestPaths sp, int src){
    //recursively counts the total number of shortest paths from a given node to the source of sp
	int count = 0;
	if(sp.pred[src] == NULL) return 0;
	PredNode *p = sp.pred[src];
	while(p != NULL){
		count += (p->v == sp.src) ? 1+countPaths(sp,p->v) : countPaths(sp,p->v);
		p = p->next;
	}
	return count;
}


int countPathsThroughv(ShortestPaths sp, int src, int v, int foundV){
	//recursively counts the total number of shortest paths from a given node to the source of sp, that pass through node v
    int count = 0;
	if(sp.pred[src] == NULL) return 0;
	PredNode *p = sp.pred[src];
	while(p != NULL){
		int newfoundV=foundV;
        if(p->v==v)newfoundV=1;
        count += (p->v == sp.src && foundV==1) ? 1+countPathsThroughv(sp,p->v,v,newfoundV) : countPathsThroughv(sp,p->v,v,newfoundV);
		p = p->next;
	}
	return count;
}

void  showNodeValues(NodeValues n){
 	int i;
 	for(i=0; i<n.noNodes; i++){
 		printf("%d: %lf\n",i,n.values[i]);
 	}
 }
 
 void  freeNodeValues(NodeValues n){
 	free(n.values);
 }




