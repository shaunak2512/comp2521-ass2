#ifndef GRAPH
	#define GRAPH
	#include "Graph.h"
#endif
#include "GraphVis.h"
#include <stdlib.h>
#include <stdio.h>

int main(){
	// some very simple code to show you
	// how to use graph vis

  Graph g = newGraph(6);
	insertEdge(g,0,3,1);
	insertEdge(g,1,2,2);
	insertEdge(g,2,1,5);
	insertEdge(g,2,4,2);
	insertEdge(g,4,3,3);
	insertEdge(g,3,4,4);
	insertEdge(g,3,5,1);
	insertEdge(g,5,4,3);
  //showGraph(g);

	// draws the graph
  graphVis(g, DEFAULT);
	return EXIT_SUCCESS;
}
