#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "Graph.h"

int containsEdge(Graph g, Vertex src,Vertex dest);
int containsEdgeIn(Graph g, Vertex src,Vertex dest);
Graph readGraphFromFile(char *f);

typedef struct GraphRep {
	AdjList *inAdj;
	AdjList *outAdj; //Array of Adjacency Lists
	int nV; // #Vertices
	//int nE; // #Edges
} GraphRep;

Graph newGraph(int noNodes){
	assert(noNodes >= 0);
	//printf("G\n");
	Graph g = malloc(sizeof(struct GraphRep));
	assert(g != NULL);
	g->nV = noNodes;
	//g->nE = 0;
	//Allocate memory for the array of adacency lists
	g->outAdj = malloc(noNodes * sizeof(AdjList));
	assert(g->outAdj != NULL);
	g->inAdj = malloc(noNodes * sizeof(AdjList));
	assert(g->inAdj != NULL);
	int i;
	for(i = 0; i < noNodes; i++){
		g->outAdj[i] = NULL;
		g->inAdj[i] = NULL;
	}
	return g;
}

void insertEdge(Graph g, Vertex src, Vertex dest, int weight){

	//Create a new adj list node with the given vertex and weight, for the out adjacency list
	adjListNode *newEdgeOut = malloc(sizeof(adjListNode));
	newEdgeOut->w = dest;
	newEdgeOut->weight = weight;
	newEdgeOut->next = NULL;
    
    //Create a new adj list node with the given vertex and weight, for the in adjacency list
	adjListNode *newEdgeIn = malloc(sizeof(adjListNode));
	newEdgeIn->w = src;
	newEdgeIn->weight = weight;
	newEdgeIn->next = NULL;

	//If the source node doesnt have an adjacency list, create a new one
    if(g->outAdj[src] == NULL){
            g->outAdj[src] = newEdgeOut;
    }
	else if(!containsEdge(g,src,dest)){
		AdjList l = g->outAdj[src];
		//Go all the way to the last node of source node's adjacency list
		while(l->next != NULL) 
			l = l->next;
		//Add the new edge to the source node's adjacency list
		l->next = newEdgeOut;
		//g->nE ++;
	}

	if(g->inAdj[dest] == NULL){
		g->inAdj[dest] = newEdgeIn;
	}
	else if(!containsEdgeIn(g,dest,src)){
		AdjList temp = g->inAdj[dest];
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newEdgeIn;
	}
	//printf("End\n");
}

int containsEdgeIn(Graph g, Vertex src,Vertex dest){
	//this is just a helper function

	AdjList l= g->inAdj[src];
	while(l != NULL){
		if(l->w == dest) return 1;
		l = l->next;
	}
	return 0;
}

int containsEdge(Graph g, Vertex src,Vertex dest){
	AdjList l= g->outAdj[src];
	while(l != NULL){
		if(l->w == dest) return 1;
		l = l->next;
	}
	return 0;
}

void removeEdge(Graph g, Vertex src, Vertex dest){
	assert(src < g->nV && dest < g->nV);
	assert(g->outAdj[src]);
	if(containsEdge(g,src,dest)){
		AdjList l = g->outAdj[src];
		while(l != NULL){
			if(l->next->w == dest){
				l->next = l->next->next;
			}
			l = l->next;
		}
	}	
}

bool adjacent(Graph g, Vertex src, Vertex dest){
	assert(src < g->nV && dest < g->nV);
	AdjList l = g->outAdj[src];
	while(l != NULL){
		if(l->w == dest)
			return true;
		l = l->next;
	}
	return false;
}

int numVerticies(Graph g){
	return g->nV;
}

AdjList outIncident(Graph g, Vertex v){
	AdjList l = NULL;
	if(g->outAdj[v]) return g->outAdj[v];
	else return l;
}

AdjList inIncident(Graph g, Vertex v){

	AdjList l =NULL;
	if(g->inAdj[v]) return g->inAdj[v];
	else return l;
}


void showGraph(Graph g){
	int i = 0;
	printf("Printing Graph: \n");
	for(i = 0; i<g->nV;i++){
		printf("Vertex %d has an outgoing link to : \n",i);

		AdjList l = g->outAdj[i];
		while(l !=  NULL){
			printf("\t%d : w = %d\n",l->w,l->weight);
			l = l->next;
		}

		printf("Vertex %d has ingoing link to: \n", i);
		AdjList list = g->inAdj[i];
		while(list != NULL){
			printf("\t%d : w = %d\n",list->w,list->weight);
			list = list->next;
		}
		printf("\n");
	}
}	

void freeGraph(Graph g){
	int i =0;
	//Free adacency lists of all vertices
	for(i = 0;i < g->nV;i++){
		//Free individual nodes in the adacency list
			adjListNode *n = g->outAdj[i];
			while(n != NULL){
				adjListNode * temp =n;
                n = n->next;
                free(temp);
			}
            n = g->inAdj[i];
			while(n != NULL){
				adjListNode * temp =n;
                n = n->next;
                free(temp);
			}
	}
	if(g->outAdj) free(g->outAdj);
	if(g->inAdj) free(g->inAdj);
	if(g)free(g);


}






















