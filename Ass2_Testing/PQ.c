#include "PQ.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

int isItemInPQ(PQ q, ItemPQ item);


typedef struct PQNode {
	ItemPQ item;
	struct PQNode *next;
} PQNode; 

typedef struct PQRep {
	PQNode *head;
} PQRep;


PQNode *newNode(ItemPQ item){
	PQNode *n = malloc(sizeof(PQNode));
	n->item = item;
	n->next = NULL;
	return n;
}

ItemPQ newItem(int k, int v){
	ItemPQ item;
	item.key = k;
	item.value = v;
	return item;

}

PQ newPQ(){
	PQ q = malloc(sizeof(PQRep));
	q->head = NULL;
	return q;
}

void addPQ(PQ q, ItemPQ item){

	PQNode *new = newNode(item);
	if(PQEmpty(q)){
		q->head = new;
	}
	else{
		if(isItemInPQ(q,item)){
            free(new);
			updatePQ(q,item);
		}
	//If item's key exists in PQ update its value
		else{
		
		//If item's value is less than head's value, insert item ust before head in PQ
			if(item.value < q->head->item.value){
				new->next = q->head;
				q->head = new;
			}
			else{
				PQNode *curr = q->head;
				// Traverse the list and find a
		        // position to insert new node
				while(curr->next != NULL && curr->next->item.value <= new->item.value){
					curr = curr->next;
				}
				// Either at the end of the list
		        // or at required position
		        new->next = curr->next;
		        curr->next = new;
			}
		}	
	}	
}

ItemPQ dequeuePQ(PQ q){
	assert(!PQEmpty(q));
	//gets a pointer to the head of the queue
    PQNode *n = q->head;
	q->head = q->head->next;
    
    //copies the item into the stack
    ItemPQ temp = n->item;
    //frees the memory where the item was stored
    free(n);
	return temp;
}

int isItemInPQ(PQ q, ItemPQ item){
	//loops through the queue and returns 1 if an item exists and 0 otherwise.
    PQNode *curr = q->head;
	while(curr != NULL){
		if(curr->item.key == item.key) return 1;
		curr = curr->next;
	}
	return 0;
}

void  updatePQ(PQ q, ItemPQ item){
	PQNode *curr = q->head;
	//If only one item in q whose value needs to change
	if(curr->item.key == item.key && curr->next == NULL){
		curr->item.value = item.value;
	}
	//Otherwise we go through all nodes and delete the node with same key as item.key
    //we then add the the item to the queue again with its new value
	
	else{
		PQNode *prev = NULL;
		while(curr != NULL){
			if(curr->item.key == item.key){
				//If first item needs to be deleted
				if(prev == NULL){
					q->head = curr->next;
					free(curr);
					addPQ(q,item);
					break;
				}
				else{
					prev->next = curr->next;
					free(curr);
					addPQ(q,item);
					break;
				}
			}
			prev = curr;
			curr = curr->next;
		}
	}	
}

int PQEmpty(PQ q){
	return (q == NULL || q->head == NULL)? 1 : 0;
}

void  showPQ(PQ q){
	if(PQEmpty(q)) printf("PQ is empty\n");
	else {
		PQNode *curr = q->head;
		printf("Printing priority queue: \n");
		while(curr != NULL){
			printf("\tKey: %d ; Value: %d\n",curr->item.key,curr->item.value);
			curr = curr->next;
		}
		printf("End printing priority queue\n");
	}	
}

void freePQ(PQ q){
	PQNode *curr = q->head;
	// loops through the queue nodes, freeing them
    while(curr != NULL){
		PQNode *temp = curr;
		free(temp);
		curr = curr->next;
	}
	q->head = NULL;
    //frees the q rep structure
	free(q);
}


/*int main(int argc, char const *argv[])
{
	PQ q = newPQ();
	addPQ(q,newItem(2,6));
	showPQ(q);
	addPQ(q,newItem(2,4));
	showPQ(q);
	addPQ(q,newItem(7,20));
	showPQ(q);
	addPQ(q,newItem(2,3));
	showPQ(q);
	addPQ(q,newItem(5,2));
	showPQ(q);
	addPQ(q,newItem(8,9));

	//freePQ(q);
	showPQ(q);
	addPQ(q,newItem(9,3));
	showPQ(q);
	return 0;
}*/

