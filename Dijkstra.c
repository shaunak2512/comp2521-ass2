#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "Dijkstra.h"
#include "PQ.h"
#include <limits.h>

void tracePath(ShortestPaths sp, int pred[], int j);
void traceSolution(ShortestPaths sp,int pred[]);

void freePredsList(PredNode *n){
    //loops through the pred list, freeing each node
    while(n != NULL){
			PredNode* temp = n;
			n = n->next;
			free(temp);
		}
}

ShortestPaths dijkstra(Graph g, Vertex src){
	int nV = numVerticies(g);

	ShortestPaths sp;
	sp.noNodes = nV;
	sp.src = src;
	sp.dist = malloc(nV * sizeof(int));
    sp.pred = malloc(sizeof(PredNode*) * nV);
	
    int i;
	for(i=0; i<nV; i++) {
		//setting the pred lists to point to NULL
        sp.pred[i] = NULL;
	}

	//Set all dist[i] to INF except dist[src] which is 0
	for(i=0; i<nV; i++) {
		if(i == src) sp.dist[i] = 0;
		else sp.dist[i] = INT_MAX;	
	}

	PQ q = newPQ();

	//Add all vertices to the PQ
	for(i=0; i<nV; i++){
		ItemPQ item ;
		item.key = i;
		item.value = sp.dist[i];
		addPQ(q,item);
	}
	

	while(!PQEmpty(q)){
		//pops an item off the front of the queue
        ItemPQ curr = dequeuePQ(q);
		
		AdjList l = outIncident(g,curr.key);
		while(l != NULL){
			unsigned int tent_d = sp.dist[curr.key] + l->weight;
			
			//Relax
			if(tent_d <= sp.dist[l->w] && l->w != src){
				ItemPQ temp;
				temp.key = l->w;
				temp.value = tent_d;
				addPQ(q,temp);
				
                //makes a pred node n with the current node as v
				PredNode *n = malloc(sizeof(struct PredNode));
				n->v = curr.key;
				n->next = NULL;
				
				if(tent_d < sp.dist[l->w]){
                    //if the new tentative distance is less than what we had before we need to clear the pred list and add n as the only predecessors
                    freePredsList(sp.pred[l->w]);
                    sp.pred[l->w]=n;
                }else{
                    //this means that the tent_d equals the lowest we have had before, so we just add n to the list of predecessors
                    PredNode * curr=sp.pred[l->w];
                    while(curr->next!=NULL)curr=curr->next;
                    curr->next = n;
                }
                //we set the lowest distance to be the new tent_d we found
                sp.dist[l->w] = tent_d;
			}
			l = l->next;
           
		} 
		sp.pred[src] = NULL;
		
	}
	//setting the distance to zero for unreachable nodes
	for(i=0; i<sp.noNodes; i++){
		if(sp.dist[i] > (INT_MAX-(INT_MAX/4))) sp.dist[i] = 0;
    }
    
    freePQ(q);
    
	return sp;
}

void  showShortestPaths(ShortestPaths sps){
	int i = 0;
	printf("Node %d\n",sps.src);
	printf("  Distance\n");
	for (i = 0; i < sps.noNodes; i++) {
			if(i == sps.src)
	    	printf("    %d : X\n",i);
			else
				printf("    %d : %d\n",i,sps.dist[i]);
	}
	printf("  Preds\n");
	for (i = 0; i < sps.noNodes; i++) {
		printf("    %d : ",i);
		PredNode* curr = sps.pred[i];
		while(curr!=NULL) {
			printf("[%d]->",curr->v);
			curr = curr->next;
		}
		printf("NULL\n");
	}

}

void freeShortestPaths(ShortestPaths sp){
	int i;
	for(i=0;i<sp.noNodes;i++){
		//frees the pred list for each node
        freePredsList(sp.pred[i]);
	}
    //frees the pred array itself
	if(sp.pred) free(sp.pred);
    //frees the distance node
	if(sp.dist) free(sp.dist);
}
